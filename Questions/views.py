from django.shortcuts import render, redirect, get_object_or_404
from .models import Comment, Question
from .form import QuestionForm, UserRegistrationForm, CommentForm
from django.contrib import messages
from django.core.paginator import Paginator


# Create your views here.

def index(request):
    form = QuestionForm()
    question = Question.objects.order_by('-publish_date').all()
    paginator = Paginator(question, 4)
    page = request.GET.get('page')
    posts = paginator.get_page(page)
    if request.method == 'POST':
        form = QuestionForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Submitted Successfully.')
        else:
            messages.error(request, 'Error in submission. Try Again..')
    context = {
        'posts': posts,
        'form': form,
    }
    return render(request, 'index.html', context)


def signup(request):
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST, request.FILES)

        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Account Created Successfully for {username} !')
            return redirect('login')
        else:
            messages.error(request, 'Error in creating the account.Try again')
    else:
        form = UserRegistrationForm()

    context = {
        'form': form
    }

    return render(request, 'registration/signup.html', context)


def comments(request, id):
    questions = get_object_or_404(Question, id=id)
    content = Comment.objects.filter(questions=questions).order_by('-id')
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = request.POST.get('content')
            answer = Comment.objects.create(questions=questions, users=request.user, comments=comment)
            answer.save()
    else:
        form = CommentForm()
    context = {
        'comments': content,
        'form': form,
    }
    return render(request, 'comments.html', context)
