from .models import Question, Comment
from django.forms import ModelForm
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class UserRegistrationForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password1', 'password2']


class QuestionForm(ModelForm):
    class Meta:
        model = Question
        fields = ['question']


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['comments']
