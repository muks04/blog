from django.db import models
from django.utils.timezone import now
from ckeditor.fields import RichTextField
from django.contrib.auth.models import User


class Question(models.Model):
    question = RichTextField()
    publish_date = models.DateTimeField(default=now, editable=False)

    def __str__(self):
        return self.question


class Comment(models.Model):
    comments = models.TextField()
    users = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    questions = models.ForeignKey(Question, on_delete=models.CASCADE)
    comment_date = models.DateTimeField(default=now, editable=False)
    approved_comment = models.BooleanField(default=False)

    def approve(self):
        self.approved_comment = True
        self.save()

    def __str__(self):
        return '{}-{}'.format(self.questions.question, str(self.users.username))
