from django.urls import path
from .views import index, signup, comments


app_name = 'Questions'

urlpatterns = [
    path('', index, name='index'),
    path('signup', signup, name='signup'),
    path('<int:id>/', comments, name='comments')
]
